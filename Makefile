ROOT_PATH=$(shell pwd)

env = local

# 编译proto
.PHONY: local
local:
	go build -o protoc-gen-ts.exe main.go
	cp protoc-gen-ts.exe /d/gopath/bin