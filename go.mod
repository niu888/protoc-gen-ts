module gitee.com/niu888/protoc-gen-ts

go 1.18

require (
	github.com/golang/glog v1.0.0
	github.com/golang/protobuf v1.5.2
	github.com/google/go-cmp v0.5.9
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.14.0
	golang.org/x/text v0.4.0
	google.golang.org/genproto v0.0.0-20221114212237-e4508ebdbee1
	google.golang.org/protobuf v1.28.1
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/kr/text v0.2.0 // indirect
	golang.org/x/net v0.2.0 // indirect
	golang.org/x/sys v0.2.0 // indirect
	google.golang.org/grpc v1.50.1 // indirect
)
