package lib

import (
    "strings"
    "unicode"
)

func Ucfirst(str string) string {
    for i, v := range str {
        return string(unicode.ToUpper(v)) + str[i+1:]
    }
    return ""
}

// FirstLower 首字母小写
func FirstLower(str string) string {
    for i, v := range str {
        return string(unicode.ToLower(v)) + str[i+1:]
    }
    return ""
}

func PathToKey(path string) string {
    pathArr := strings.Split(path, "/")
    var key string
    for i, v := range pathArr {
        if i == 0 {
            key = v
        } else {
            key += Ucfirst(v)
        }
    }
    return key
}

// 替换字符串中的换行符
func ReplaceNewLine(str string) string {
    str = strings.Replace(str, "\n", " ", -1)
    str = strings.Replace(str, "\r", " ", -1)
    return str
}
